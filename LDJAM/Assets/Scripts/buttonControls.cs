﻿using UnityEngine;
using System.Collections;

public class buttonControls : MonoBehaviour {

	public Sprite normal;
	public Sprite hover;
	public Sprite click;

	public int scene = 0;

	SpriteRenderer render;

	void Start(){
		render = GetComponent<SpriteRenderer> ();
	}

	void Update(){
		//render.sprite = normal;
	}

	void OnMouseOver(){
		if(!Input.GetKey(KeyCode.Mouse0)){
			render.sprite = hover;
		}
	}
	void OnMouseExit(){
		render.sprite = normal;
	}
	void OnMouseDown(){
		render.sprite = click;
	}

	void OnMouseUp(){
		if (scene < 0) {
			Application.Quit ();
			Debug.Log ("quit");
		}
		Application.LoadLevel (scene);
	}
}
