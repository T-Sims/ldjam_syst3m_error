﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {


	Vector3 velocity = new Vector3(0,0,0); //players current speed
	float maxSpeed = 0.1f; //players movement speed
	float accel = .02f;

	bool hiding = false; //check to see if the charachter is in disguise
	float hideTimeStarting = 1000f; //the amount of time the player can hide
	float hideTimeCurrent;
	bool canSprint = true;

	SpriteRenderer render;


	public Image hide;

	Animator anim;


	AudioSource Player;
	public AudioClip[] SFX;


	// Use this for initialization
	void Start () {
		hideTimeCurrent = hideTimeStarting;
		anim = GetComponent<Animator> ();
		Player = GetComponent<AudioSource> ();
		//FindClip ();
		Player.clip = SFX[0];
	}
	
	// Update is called once per frame
	void Update () {
		move ();
		PlaySFX ();
		if(Input.GetKeyDown(KeyCode.Space)){
			hiding = !hiding;
		}
		if (hiding) {
			hideTimeCurrent-= 3;
			this.tag = "Human";
		} else {
			hideTimeCurrent ++;
			if (hideTimeCurrent > 1000) {
				hideTimeCurrent = 1000;
			}
			this.tag = "Monster";
		}
		if (hideTimeCurrent <= 0) {
			hiding = false;
		}

		handleAnimationState (hiding);


		hide.GetComponent <RectTransform>().sizeDelta = new Vector2 ((hideTimeCurrent/hideTimeStarting)*290,15);
			
	}

	void handleAnimationState(bool h){



		anim.enabled = true;

//		Debug.Log (anim.GetCurrentAnimatorStateInfo(0).IsName("ProtagMonsterWalk_Spreadsheet2_0"));

		anim.SetBool("Transformed", h);

		if (this.velocity.x > 0) {
			anim.SetInteger ("state", 2);
		} else if (this.velocity.x < 0) {
			anim.SetInteger ("state", 4);
		} else if (this.velocity.y < 0) {
			anim.SetInteger ("state", 3);
		} else if (this.velocity.y > 0) {
			anim.SetInteger ("state", 1);
		} else if(this.velocity.x == 0 && this.velocity.y == 0){
			anim.enabled = false;
		}
	}

	void move(){

		if (Input.GetKey (KeyCode.W)) {
			this.velocity.y += accel;
			if (this.velocity.y > maxSpeed) {
				this.velocity.y = maxSpeed;
			}
		} else if (Input.GetKey (KeyCode.S)) {
			this.velocity.y -= accel;
			if (this.velocity.y < -maxSpeed) {
				this.velocity.y = -maxSpeed;
			}
		} else 
			if (this.velocity.y > 0) {
				this.velocity.y -= accel;
			}
			else if (this.velocity.y < 0) {
				this.velocity.y += accel;
			}
		

		if (Input.GetKey (KeyCode.A)) {
			this.velocity.x -= accel;
			if (this.velocity.x < -maxSpeed) {
				this.velocity.x = -maxSpeed;
			}
		} else if (Input.GetKey (KeyCode.D)) {
			this.velocity.x += accel;
			if (this.velocity.x > maxSpeed) {
				this.velocity.x = maxSpeed;
			}
		} else {
			if (this.velocity.x > 0) {
				this.velocity.x -= accel;
			}
			else if (this.velocity.x < 0) {
				this.velocity.x += accel;
			}
		}

		if(Input.GetKeyDown(KeyCode.LeftShift) && canSprint){
			if(this.velocity.x != 0){
				this.velocity.x *= 4;
				canSprint = false;
			}
			if(this.velocity.y != 0){
				this.velocity.y *= 4;
				canSprint = false;
			}
		}

		if (this.velocity.x == 0 && this.velocity.y == 0) {
			canSprint = true;
		}





		this.transform.position += velocity;
	}

	void FindClip(){
		switch(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name){
			case "<Name 1>":
				Player.clip = SFX [0];
				break;
			case "<NAME 2>":
				Player.clip = SFX [1];
				break;
			case "<NAME 3>":
				Player.clip = SFX [2];
				break;
			default:
				Player.clip = SFX [0];
				break;
		}
	}

	void PlaySFX(){
		if (velocity == Vector3.zero) {
			Player.Stop ();
		} else {
			Player.Play ();
		}
	}
		
}
