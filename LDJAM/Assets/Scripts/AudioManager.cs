﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	private AudioSource Player;

	public AudioClip[] Music;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (transform.gameObject);
		Player = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetClip(string name){
		switch(name){
		case "Title":
			Player.clip = Music[0];
			Player.Play ();
			break;
		case "Level 1":
			Player.clip = Music [1];
			Player.Play ();
			break;
		case "Town":
			Player.clip = Music[2];
			Player.Play ();
			break;
		case "Final":
			Player.clip = Music[3];
			Player.Play ();
			break;
		case "GameOver":
			Player.clip = Music[4];
			Player.Play ();
			break;
		default:
			Player.Stop ();
			break;
		}
	}

	public void PauseClip(){
		Player.Pause ();
	}

	public void ResumeClip(){
		Player.UnPause ();
	}
}
