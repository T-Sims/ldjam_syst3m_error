﻿using UnityEngine;
using System.Collections;

public class EnemyDetection : MonoBehaviour {



	public float distance = 5.0f;
	public float angle = 45;
	int segments = 10;

	public Vector2 direction;
	public Vector3 targetPos;

	// Use this for initialization
	void Start () {
		angle = (angle * Mathf.PI) / 180;
	}
	
	// Update is called once per frame
	void Update () {

		raycastSweep ();
	
	}

	void raycastSweep(){


		Vector3 startPos = this.transform.position;
		targetPos = this.transform.position;

		float startAngle = (float)-angle/2;
		float finishAngle = (float)angle/2;

		float zPos = this.transform.eulerAngles.z;
		zPos = (zPos * Mathf.PI) / 180;



		if (zPos > Mathf.PI) {
			zPos -= Mathf.PI;
			zPos *= -1f;
		} else {
			zPos -= Mathf.PI;
			zPos *= -1;
		}


			




		startAngle -= zPos;
		finishAngle -= zPos;



		float inc = (float)angle / segments;

		RaycastHit2D hit;

		for (float i = startAngle; i < finishAngle; i += inc) {


			float posX = Mathf.Sin (i)*distance;
			float posY = Mathf.Cos (i)*distance;

			posX += this.transform.position.x;
			posY -= this.transform.position.y;

			targetPos = new Vector3 (posX, posY, 0);
			if (zPos < 0) {
				targetPos = new Vector3 (posX, -posY, 0);
			}

			direction = (targetPos - startPos).normalized;

			float dist = (targetPos - startPos).magnitude;

			//hit = Physics2D.Raycast (startPos, direction, distance);
			hit = Physics2D.Linecast(startPos, targetPos);
			if (hit.collider != null) {
				//Debug.Log (hit.point);
				//Debug.DrawRay (startPos, (Vector3)hit.point-startPos, Color.green);
				Debug.DrawLine(startPos,targetPos,Color.white);
				if (hit.collider.gameObject.tag == "Monster") {
					Application.LoadLevel (6);
				}
			} else {
				//Debug.Log (targetPos);
				//Debug.DrawRay (startPos, targetPos-startPos, Color.red);
				Debug.DrawLine(startPos,targetPos,Color.blue);
			}

			//Debug.Log ("startpos is: " + startPos + " endPos is " + targetPos + " dist is : " +dist);
			
		}
	}

}
