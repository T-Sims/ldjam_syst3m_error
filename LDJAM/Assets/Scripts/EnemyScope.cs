﻿using UnityEngine;
using System.Collections;

public class EnemyScope : MonoBehaviour {

	public bool isOrbiting;
	public float angle;
	public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(isOrbiting){
			transform.RotateAround (transform.parent.position, new Vector3(0,0,1), 25 * Time.deltaTime);
		}
		transform.RotateAround (transform.parent.position, new Vector3(0,0,1), angle * speed);
	}
}
