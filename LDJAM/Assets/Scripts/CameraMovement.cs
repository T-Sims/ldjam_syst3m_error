﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {


	public static int ROTATE_FOR = 45;
	int rotated = ROTATE_FOR;
	public float turnSpeed = .05f;

	bool ascend = true;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (ascend) {
			transform.Rotate (Vector3.forward *  -turnSpeed);
		} else {
			transform.Rotate (Vector3.forward *  turnSpeed);
		}

		rotated--;

		if (rotated < 0) {
			ascend = !ascend;
			rotated = ROTATE_FOR;
		}
	
	}
}
