﻿using UnityEngine;
using System.Collections;

public class FlashlightBehavior : MonoBehaviour {


	public float speed = .5f;
	public int sweep = 10;

	int swept = 0;
	bool right;



	// Use this for initialization
	void Start () {

	
	}




	void OnTriggerStay2D(Collider2D cInfo){
		//Debug.DrawLine (transform.position, cInfo.transform.position, Color.green);

		RaycastHit2D hit = Physics2D.Linecast (this.transform.position, cInfo.transform.position);

		if (hit.collider != null) {
			if (hit.transform.tag == "Monster") {
				Application.LoadLevel (6);
			}
		}
	}
	void OnTriggerEnter2D(Collider2D cInfo){
	}

	// Update is called once per frame
	void Update () {



		swept++;
		if (right) {
			transform.Rotate (Vector3.forward *  speed);
		}else{
			transform.Rotate (Vector3.forward *  -speed);
		}

		if (swept > sweep) {
			right = !right;
			swept = 0;
		}

	}
}
