﻿using UnityEngine;
using System.Collections;

public class CameraTrack : MonoBehaviour {

	public GameObject target;
	public int buffer = 50;
	public Camera camera;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 pos = camera.WorldToScreenPoint(target.transform.position);
		var targetPos = new Vector3 (target.transform.position.x, target.transform.position.y, -10);

		if (pos.x < buffer || pos.x > camera.pixelWidth - buffer) {
			this.transform.position = Vector3.Lerp (this.transform.position, targetPos, .03f);
		}
		if (pos.y < buffer || pos.y > camera.pixelHeight - buffer) {
			this.transform.position = Vector3.Lerp (this.transform.position, targetPos, .03f);
		}

	
	}
}
