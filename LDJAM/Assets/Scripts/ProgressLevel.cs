﻿using UnityEngine;
using System.Collections;

public class ProgressLevel : MonoBehaviour {

	public int scene;

	// Use this for initialization
	void OnCollisionEnter2D(){
		if (scene != null) {
			Application.LoadLevel (scene);
		} else {
			int level = Application.loadedLevel;
			Application.LoadLevel (level += 2);
		}
	}
}
