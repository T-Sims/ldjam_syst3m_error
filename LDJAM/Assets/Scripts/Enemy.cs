﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	/**
	 * Patrolling enemy that moves between two points. 
	 * Contains a cone shaped "camera" to view with.
	 * On launch an endgame (console) event on collision with player.
	 * Also rotates between two angles.
	 **/
	private Vector3 startPoint;

	private Vector3 velocity;
	private bool patroling = true;
	private GameObject EndTarget;

	private float traveled;
	private float angle;

	public Vector3 endPoint;
	public Vector3[] navPoints;
	public GameObject[] navObjects;
	public int index;
	public float speed;
	public GameObject target;
	public GameObject light;

	Vector3 lastPosition;

	Animator anim;
	// Use this for initialization
	void Start () {
		lastPosition = this.transform.position;
		anim = GetComponent<Animator> ();
		startPoint = transform.position;
		StartCoroutine ("Patrol");
		PointAt (endPoint);
		for(int i = 0; i < navObjects.Length; i++){
			navPoints [i] = navObjects [i].transform.position;
		}

	}

	void animStateChange(){
		float changeX = this.transform.position.x - lastPosition.x;
		float changeY = this.transform.position.y - lastPosition.y;

		if (Mathf.Abs (changeX) > Mathf.Abs (changeY)) {
			if (changeX >= 0) {
				anim.SetInteger ("direction", 2);
			} else {
				anim.SetInteger ("direction", 4); 
			}
		} else {
			if (changeY >= 0) {
				anim.SetInteger ("direction", 1);
			} else {
				anim.SetInteger ("direction", 3); 
			}
		}


		lastPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		animStateChange ();

		transform.position = Vector3.Lerp (transform.position, endPoint, 0.1f * speed * Time.deltaTime);
		/*
		if(Vector3.Distance(transform.position, endPoint) < 0.05f){
			Vector3 newEnd = startPoint;
			startPoint = transform.position;
			endPoint = newEnd;

			Debug.Log (startPoint + ", " + endPoint);
		}
		*/
	}

	void Route(Vector3 target){
		endPoint = target;
	}

	void PointAt(Vector3 target){
		Vector3 direction = target - light.transform.position;
		angle = (Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg) - 90f;
		light.transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	IEnumerator Patrol(){
		while(patroling){
			
			if(Vector3.Distance(transform.position, endPoint) < 0.5f){

				if (index > navPoints.Length-1) {
					index = 0;
				}

				endPoint = navPoints [index];
				//Debug.Log (index);
				PointAt (endPoint);
				index++;

				/*Vector3 newEnd = startPoint;
				startPoint = transform.position;
				endPoint = newEnd;*/
				//PointAt (endPoint);
				//Debug.Log (startPoint + ", " + endPoint);
			}
			yield return null;
		}
	}
		
}